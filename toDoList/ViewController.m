//
//  ViewController.m
//  toDoList
//
//  Created by Prince on 30/09/15.
//  Copyright (c) 2015 Prince. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{ NSMutableArray *data;
}
@property (strong, nonatomic) IBOutlet UIButton *add;
@property (strong, nonatomic) IBOutlet UIButton *ok;
@property (strong, nonatomic) IBOutlet UITextField *textField;
@property (strong, nonatomic) IBOutlet UITableView *tableView1;
@property (strong, nonatomic) IBOutlet UILabel *label;

@end

@implementation ViewController
@synthesize tableView1;


- (void)viewDidLoad {
    data=[[NSMutableArray alloc]init];
    _textField.hidden = TRUE;
    _ok.hidden = TRUE;
    
       [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [data count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableView1 dequeueReusableCellWithIdentifier:@"identifier"];
    cell.textLabel.text=data[indexPath.row];
    return cell;
}

-(IBAction)pluspressed{
    
    _textField.hidden = FALSE;
    _ok.hidden = FALSE;
    



}


-(IBAction)okPressed{
    
    [data addObject:_textField.text];
    [tableView1 reloadData];
    
    self.textField.text=@"";
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
